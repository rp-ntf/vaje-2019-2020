#include <stdio.h>

/*
 * Sestavite program, ki sesteje 10 od nic razlicnih stevil, ki jih poda uporabnik,
 * in izpise njihovo vsoto
 * 
 * Write a program that sums 10 numbers, not equal to zero, input by the user, and prints out their sum
 */

int main(int argc, char **argv)
{
	int  n=0;
    int vsota=0;
    int x;
    
    while(n<10)
    {
        printf("x = "); scanf("%d",&x);
        if (x==0) continue;
        vsota = vsota + x;
        n++;
    }
    
    printf("Vsota 10 od nic razlicnih stevil je %d.\n\n",vsota);
	return 0;
}
