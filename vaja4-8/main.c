#include <stdio.h>

/*
 * Sestavite program, ki na zaslon izpise iz zvezdic sestavljen pravokotnik dimenzij m x n,
 * m in n prebere od uporabnika.
 * 
 * Write a program that prints a rectangle made of "*" characters on screen, with dimensions m x n,
 * m and n are read from the user
 * 
 * primer / example :
 * m = 2, n = 3
 
**
**
**

 
 */

int main(int argc, char **argv)
{
	int m,n;
    
    printf("Vpisi m: "); scanf("%d",&m);
    printf("Vpisi n: "); scanf("%d",&n);
    
    for (int i = 0; i<m;i++) {
        for(int j = 0; j < n; j++) {
            printf("*");
        }
        printf("\n");
    }
	return 0;
}
