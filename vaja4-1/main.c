/* Program izracuna vsoto prvih n clenov vrste za exp(x),
 * n in x vpise uporabnik 
 * 
 * Write a program that calculates the sum of first n terms in the series expansion for exp(x) function,
 * n and x is given by the user
 * 
 * Exp vrsta (exp series) : https://en.wikipedia.org/wiki/Taylor_series#Exponential_function
 * */
#include <stdio.h>
#include<math.h>

int main(int argc, char **argv)
{
	float x;
    int n;
    float trVrednost=1.0;
    float vsota=trVrednost;
    
    printf("Vnesi stevilo x: "); scanf("%f",&x);
    printf("Vnesi stevilo clenov: "); scanf("%d",&n);
    
    for (int i=1;i<n;i++) {
        trVrednost=trVrednost*x/i;
        vsota+=trVrednost;    // vsota = vsota + trVrednost;
    }
    
    printf("Izracunana vrednost je %g, prava vrednost pa je %g.\n\n",vsota,exp(x));
    
	return 0;
}
